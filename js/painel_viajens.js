var url = 'http://' + QLIK_HOST + ':' + QLIK_PORT + '/resources/assets/external/requirejs/require.js';
var config = {
    host: "qliksense-qap02.brazilsouth.cloudapp.azure.com",
    prefix: "/",
    port: "80",
    isSecure: false
};

require.config({
    baseUrl: (config.isSecure ? "https://" : "http://" ) + config.host + (config.port ? ":" + config.port : "") + config.prefix + "resources",
    paths: {handlebars: "//cdnjs.cloudflare.com/ajax/libs/handlebars.js/3.0.3/handlebars.min"} // NecessÃ¡rio para criar o cubo.
});

require(["js/qlik"], function (qlik) {

    qlik.setOnError(function (error) {
        console.log(error.message);
    });

    app = qlik.openApp(QLIK_APP_ID, config);
    carregarTela();
});

function carregarTela() {

    //FILTROS

            app.visualization.get(QS_FILTRO_01).then(function (vis) {vis.show("QVFILTRO01");});
            app.visualization.get(QS_FILTRO_02).then(function (vis) {vis.show("QVFILTRO02");});
            app.visualization.get(QS_FILTRO_03).then(function (vis) {vis.show("QVFILTRO03");});
            app.visualization.get(QS_FILTRO_04).then(function (vis) {vis.show("QVFILTRO04");});
            app.visualization.get(QS_FILTRO_05).then(function (vis) {vis.show("QVFILTRO05");});
            app.visualization.get(QS_FILTRO_06).then(function (vis) {vis.show("QVFILTRO06");});
            app.visualization.get(QS_FILTRO_07).then(function (vis) {vis.show("QVFILTRO07");});
            app.visualization.get(QS_FILTRO_08).then(function (vis) {vis.show("QVFILTRO08");});
            app.visualization.get(QS_FILTRO_09).then(function (vis) {vis.show("QVFILTRO09");});
            app.visualization.get(QS_FILTRO_10).then(function (vis) {vis.show("QVFILTRO10");});
            app.visualization.get(QS_FILTRO_11).then(function (vis) {vis.show("QVFILTRO11");});

    // OBJETOS ANALISE DE VIAJEM

        //VIAJENS

            app.visualization.get(QS_TAB_ANALISE_VIAJEM_VIAJENS_01).then(function (vis) {vis.show("QVAVV01");});
            app.visualization.get(QS_TAB_ANALISE_VIAJEM_VIAJENS_02).then(function (vis) {vis.show("QVAVV02");});
            app.visualization.get(QS_TAB_ANALISE_VIAJEM_VIAJENS_03).then(function (vis) {vis.show("QVAVV03");});
            app.visualization.get(QS_TAB_ANALISE_VIAJEM_VIAJENS_04).then(function (vis) {vis.show("QVAVV04");});
            app.visualization.get(QS_TAB_ANALISE_VIAJEM_VIAJENS_05).then(function (vis) {vis.show("QVAVV05");});
            app.visualization.get(QS_TAB_ANALISE_VIAJEM_VIAJENS_06).then(function (vis) {vis.show("QVAVV06");});
            app.visualization.get(QS_TAB_ANALISE_VIAJEM_VIAJENS_07).then(function (vis) {vis.show("QVAVV07");});
            app.visualization.get(QS_TAB_ANALISE_VIAJEM_VIAJENS_08).then(function (vis) {vis.show("QVAVV08");});
            app.visualization.get(QS_TAB_ANALISE_VIAJEM_VIAJENS_09).then(function (vis) {vis.show("QVAVV09");});
            app.visualization.get(QS_TAB_ANALISE_VIAJEM_VIAJENS_10).then(function (vis) {vis.show("QVAVV10");});
            app.visualization.get(QS_TAB_ANALISE_VIAJEM_VIAJENS_11).then(function (vis) {vis.show("QVAVV11");});
            app.visualization.get(QS_TAB_ANALISE_VIAJEM_VIAJENS_12).then(function (vis) {vis.show("QVAVV12");});
            app.visualization.get(QS_TAB_ANALISE_VIAJEM_VIAJENS_13).then(function (vis) {vis.show("QVAVV13");});
            app.visualization.get(QS_TAB_ANALISE_VIAJEM_VIAJENS_14).then(function (vis) {vis.show("QVAVV14");});



        //DETALHAMENTO DE DIÁRIAS

            app.visualization.get(QS_TAB_ANALISE_VIAJEM_DET_DIARIAS_01).then(function (vis) {vis.show("QVAVDD01");});
            app.visualization.get(QS_TAB_ANALISE_VIAJEM_DET_DIARIAS_02).then(function (vis) {vis.show("QVAVDD02");});
            app.visualization.get(QS_TAB_ANALISE_VIAJEM_DET_DIARIAS_03).then(function (vis) {vis.show("QVAVDD03");});
            app.visualization.get(QS_TAB_ANALISE_VIAJEM_DET_DIARIAS_04).then(function (vis) {vis.show("QVAVDD04");});
            app.visualization.get(QS_TAB_ANALISE_VIAJEM_DET_DIARIAS_05).then(function (vis) {vis.show("QVAVDD05");});
            app.visualization.get(QS_TAB_ANALISE_VIAJEM_DET_DIARIAS_06).then(function (vis) {vis.show("QVAVDD06");});
            app.visualization.get(QS_TAB_ANALISE_VIAJEM_DET_DIARIAS_07).then(function (vis) {vis.show("QVAVDD07");});
            app.visualization.get(QS_TAB_ANALISE_VIAJEM_DET_DIARIAS_08).then(function (vis) {vis.show("QVAVDD08");});
            app.visualization.get(QS_TAB_ANALISE_VIAJEM_DET_DIARIAS_09).then(function (vis) {vis.show("QVAVDD09");});
            app.visualization.get(QS_TAB_ANALISE_VIAJEM_DET_DIARIAS_10).then(function (vis) {vis.show("QVAVDD10");});
            app.visualization.get(QS_TAB_ANALISE_VIAJEM_DET_DIARIAS_11).then(function (vis) {vis.show("QVAVDD11");});
            app.visualization.get(QS_TAB_ANALISE_VIAJEM_DET_DIARIAS_12).then(function (vis) {vis.show("QVAVDD12");});



        //DETALHAMENTO DE PASSAGENS


            app.visualization.get(QS_TAB_ANALISE_VIAJEM_DET_PASSAGENS_01).then(function (vis) {vis.show("QVAVDP01");});
            app.visualization.get(QS_TAB_ANALISE_VIAJEM_DET_PASSAGENS_02).then(function (vis) {vis.show("QVAVDP02");});
            app.visualization.get(QS_TAB_ANALISE_VIAJEM_DET_PASSAGENS_03).then(function (vis) {vis.show("QVAVDP03");});
            app.visualization.get(QS_TAB_ANALISE_VIAJEM_DET_PASSAGENS_04).then(function (vis) {vis.show("QVAVDP04");});
            app.visualization.get(QS_TAB_ANALISE_VIAJEM_DET_PASSAGENS_05).then(function (vis) {vis.show("QVAVDP05");});
            app.visualization.get(QS_TAB_ANALISE_VIAJEM_DET_PASSAGENS_06).then(function (vis) {vis.show("QVAVDP06");});
            app.visualization.get(QS_TAB_ANALISE_VIAJEM_DET_PASSAGENS_07).then(function (vis) {vis.show("QVAVDP07");});
            app.visualization.get(QS_TAB_ANALISE_VIAJEM_DET_PASSAGENS_08).then(function (vis) {vis.show("QVAVDP08");});
            app.visualization.get(QS_TAB_ANALISE_VIAJEM_DET_PASSAGENS_09).then(function (vis) {vis.show("QVAVDP09");});
            app.visualization.get(QS_TAB_ANALISE_VIAJEM_DET_PASSAGENS_10).then(function (vis) {vis.show("QVAVDP10");});



}